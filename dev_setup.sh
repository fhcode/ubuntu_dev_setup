#chrome
cd /tmp
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get -f install

#docker
sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get -y install docker-ce docker-compose

#run docker without sudo. Will need to be run for each user.
sudo groupadd docker
sudo usermod -aG docker $USER
echo 'You must restart your system before using docker without sudo'

#utils
sudo apt-get install -y vim git

#skype
curl https://repo.skype.com/data/SKYPE-GPG-KEY | sudo apt-key add -
echo "deb https://repo.skype.com/deb stable main" | sudo tee /etc/apt/sources.list.d/skypeforlinux.list
sudo apt update
sudo apt install skypeforlinux

#dropbox
sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5044912E
sudo add-apt-repository "deb http://linux.dropbox.com/ubuntu $(lsb_release -sc) main"
sudo apt update
sudo apt install dropbox

#nodeJS
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

#vscode 
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get update
sudo apt-get install -y code # or code-insiders

#postman (web client side emulator, API tester, automate testing) 
echo "Downloading and unpacking Postman... (1/5)"
wget -q https://dl.pstmn.io/download/latest/linux?arch=64 -O postman.tar.gz
tar -xzf postman.tar.gz
rm postman.tar.gz

echo "Installing to opt... (2/5)"
if [ -d "/opt/Postman" ];then
    sudo rm -rf /opt/Postman
fi
sudo mv Postman /opt/Postman

echo "Creating symbolic link... (3/5)"
if [ -L "/usr/bin/postman" ];then
    sudo rm -f /usr/bin/postman
fi
sudo ln -s /opt/Postman/Postman /usr/bin/postman

echo "Downloading .desktop file... (4/5)"
wget https://bitbucket.org/!api/2.0/snippets/dan_martin/Eexdg6/330c653e42270488062008751907be1e2ec32528/files/Postman.desktop

echo "Installing .desktop file... (5/5)"
if [ -e "/usr/share/applications/Postman.desktop" ];then
    sudo rm /usr/share/applications/Postman.desktop
fi
sudo mv Postman.desktop /usr/share/applications/Postman.desktop

echo "Installation completed successfully."

#OTHER POSSIBLE STUFFS
#bash stuff
#sudo apt-get install -y terminator guake 
#git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
#~/.bash_it/install.sh
#echo 'export BASH_IT_THEME="powerline"' >> ~/.bash_profile

#show hidden startup apps
#sudo sed -i "s/NoDisplay=true/NoDisplay=false/g" /etc/xdg/autostart/*.desktop
#echo 'Run startup application to remove unnecessary startups apps and services'

#yarn (facebooks npm for nodeJS-javascript)
#curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
#echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
#sudo apt-get update
#sudo apt-get install --no-install-recommends yarn

#nodejs common global packages and other package managers
#npm install -g jspm 
#npm install -g gulp
#npm install -g grunt-cli
#npm install -g bower
#npm install -g phantomjs

#git global config
#git config --global user.name "YOUR NAME"
#git config --global user.email "EMAIL"


